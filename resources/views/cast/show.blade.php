@extends('master')

@section('judul')
    Cast Show {{$cast->nama}}
@endsection

@section('content')

<h3>{{$cast->nama}}</h3>
<p>
    Umur: {{$cast->umur}}<br>
    Bio:<br>
    {{$cast->bio}}
</p>

<a href='/cast' class='btn btn-secondary'>Kembali</a>

@endsection