<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card) */
        # code...
        $cast = Cast::all();

        return view('cast.index', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /* menampilkan form untuk membuat data pemain film baru */
        # code...
        return view('cast.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* menyimpan data baru ke tabel Cast */
        # code...
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = new Cast;

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();
        
        return redirect('/cast/create');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* menampilkan detail data pemain film dengan id tertentu */
        # code...
        $cast = Cast::findOrFail($id);

        return view('cast.show', compact('cast'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /* menampilkan form untuk edit pemain film dengan id tertentu */
        # code...
        $cast = Cast::findOrFail($id);

        return view('cast.edit', compact('cast'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        /* menyimpan perubahan data pemain film (update) untuk id tertentu */
        # code...
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = Cast::find($id);

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->update();

        return redirect('/cast');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /* menghapus data pemain film dengan id tertentu */
        # code...
        $cast = Cast::find(1);
        
        $cast->delete();
        
        return redirect('/cast');
    }
}
